
#include <stdint.h>
#include <stdbool.h>

typedef struct{
	uint8_t sec;        //saniye                /* 0..59 */
	uint8_t min;        //dakika                /* 0..59 */    
	uint8_t hour;       //saat                  /* 0..23 */
	uint8_t mday;       //Ayın Günü             /* 1.. 31 */          
	uint8_t month;      //Kaçıncı Ay            /* 1..12 */
	uint8_t year;       //yıl                   /*00..99 */
	uint8_t wday;       //Haftanın günü         /* 0..6, Sunday = 0*/
	uint8_t yday;       //Yılın Kaçıncı günü    /* 1..366 */
	uint8_t isdst;      // DST Flag  0 ise daylight saving time not effect  /* 0 Winter, !=0 Summer */
    int     gmt;        // Time Zone    -12 through 0 (GMT) to +12.   
}RTC_TIME;
extern RTC_TIME Rtc_time;



void test2();

enum {
    SUNDAY=0,MONDAY=1,TUESDAY=2,WEDNESDAY=3,THURSDAY=4,FRIDAY=5,SATURDAY=6
};

enum {
    JANUARY=1,FEBRUARY=2,MARCH=3,APRIL=4,MAY=5,JUNE=6,JULY=7,AUGUST=8,SEPTEMBER=9,OCTOBER=10,NOVEMBER=11,DECEMBER=12
};

uint8_t Rtc_Get_DayOfWeek(uint16_t year,uint8_t month,uint8_t day);
uint16_t Rtc_Get_DayofYear(uint16_t year, uint8_t month, uint8_t day);

static uint8_t isDST(const RTC_TIME *t);

void test(RTC_TIME *t);