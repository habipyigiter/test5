/*
 * File:   rtc.c
 * Author: Win7
 *
 * Created on 07 Şubat 2017 Salı, 12:54
 */


#include "xc.h"
#include "rtc.h"

static __inline uint8_t isLeapYear(uint16_t year);
//static uint8_t isDST( const RTC_TIME *t );

RTC_TIME Rtc_time = {1, 2, 3, 8, 2, 17, 3, 0, 0, 2};

void test(RTC_TIME *t) {
    Nop();
    Nop();
    Nop();
    Nop();
}

void test2() {
}

static __inline uint8_t isLeapYear(uint16_t year) {
    return ((((year % 4) == 0) && ((year % 100) != 0)) || ((year % 400) == 0));
}

/*******************************************************************************
 * Function Name  : isDST
 * Description    : checks if given time is in Daylight Saving time-span.
 * Input          : time-struct, must be fully populated including weekday
 * Output         : none
 * Return         : 0 no DST ("winter"), 1 in DST ("Summer")
 *  DST according to German standard
 *  Based on code from Peter Dannegger found in the microcontroller.net forum.
 *******************************************************************************/
static uint8_t isDST(const RTC_TIME *t) {
    uint8_t hour, day, wday, month; // locals for faster access

    hour = t->hour;
    day = t->mday;
    wday = t->wday;
    month = t->month;

    Nop();
    Nop();
    Nop();

    if (month < 3 || month > 10) { // month 1, 2, 11, 12
        return 0; // -> Winter
    }

    if (day - wday >= 25 && (wday || hour >= 2)) { // after last Sunday 2:00
        if (month == 10) { // October -> Winter
            return 0;
        }
    } else { // before last Sunday 2:00
        if (month == 3) { // March -> Winter
            return 0;
        }
    }

    return 1;
}

/*Girilen Tarihin haftanın hangi Gunene Denk Geldigini Donduren Fonksiyon*/
uint8_t Rtc_Get_DayOfWeek(uint16_t year, uint8_t month, uint8_t day) {

    static char t[] = {0, 3, 2, 5, 0, 3, 5, 1, 4, 6, 2, 4};
    if (year <= 99) // y?l iki hane girilse ornek 2014=14
    {
        year = year + 2000;
    }

    year -= month < 3;
    return (year + year / 4 - year / 100 + year / 400 + t[month - 1] + day) % 7;
}

/**/
uint16_t Rtc_Get_DayofYear(uint16_t year, uint8_t month, uint8_t day) {

    if (year <= 99) {
        year = year + 2000;
    }

    uint8_t i;
    uint8_t isLeap = ((year & 3) == 0) && ((year % 100) != 0
            || (year % 400) == 0);

    static uint8_t dayList[2][13] = {
        {0, 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31},
        {0, 31, 29, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31}
    };

    for (i = 1; i < month; i++)
        day += dayList[isLeap][i];

    return day;
}





